package com.carlosgub.comunalexam.domain.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class BenefitEntity(
    @Json(name = "business_name")
    val businessName: String,
    val name: String,
    val image: String
) : Parcelable