package com.carlosgub.comunalexam.domain.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class BenefitsEntity(
    val name: String,
    val benefits: List<BenefitEntity>
):Parcelable