package com.carlosgub.comunalexam.domain.repository

import com.carlosgub.comunalexam.domain.model.BenefitsEntity
import io.reactivex.Single

interface JsonRepository {
    fun getData(): Single<List<BenefitsEntity>>
}