package com.carlosgub.comunalexam.domain.interactor

import com.carlosgub.comunalexam.core.interactor.Interactor
import com.carlosgub.comunalexam.domain.model.BenefitsEntity
import com.carlosgub.comunalexam.domain.repository.JsonRepository
import io.reactivex.Single

class GetBenefitsInteractor(
    private val jsonRepository: JsonRepository
) : Interactor<Interactor.None, Single<List<BenefitsEntity>>> {

    override fun execute(params: Interactor.None): Single<List<BenefitsEntity>> {
        return jsonRepository.getData()
    }
}