package com.carlosgub.comunalexam.data.datasource.local

import android.content.Context
import com.carlosgub.comunalexam.R
import com.carlosgub.comunalexam.domain.model.BenefitsEntity
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.get


class JsonDataStore:KoinComponent {

    private val context:Context = get()

    fun getData(): Single<List<BenefitsEntity>> {
        return Single.create { single->
            try {
                val text = context.resources.openRawResource(R.raw.data)
                    .bufferedReader().use {  it.readText() }

                val moshi = Moshi.Builder().build()
                val type  = Types.newParameterizedType(List::class.java, BenefitsEntity::class.java)
                val adapter: JsonAdapter<List<BenefitsEntity>> = moshi.adapter(type)

                adapter.fromJson(text)?.let {
                    single.onSuccess(it)
                }
            }catch (ex:Exception){
                single.onError(ex)
            }
        }
    }
}