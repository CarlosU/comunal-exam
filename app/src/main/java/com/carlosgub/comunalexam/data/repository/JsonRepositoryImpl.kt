package com.carlosgub.comunalexam.data.repository

import com.carlosgub.comunalexam.data.datasource.local.JsonDataStore
import com.carlosgub.comunalexam.domain.model.BenefitsEntity
import com.carlosgub.comunalexam.domain.repository.JsonRepository
import io.reactivex.Single

class JsonRepositoryImpl(
    private val jsonDataStore: JsonDataStore
) : JsonRepository {
    override fun getData(): Single<List<BenefitsEntity>> {
        return jsonDataStore.getData()
    }
}