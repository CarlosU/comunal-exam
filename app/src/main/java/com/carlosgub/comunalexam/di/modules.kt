package com.carlosgub.comunalexam.di

import com.carlosgub.comunalexam.core.scheduler.SchedulersImpl
import com.carlosgub.comunalexam.data.datasource.local.JsonDataStore
import com.carlosgub.comunalexam.data.repository.JsonRepositoryImpl
import com.carlosgub.comunalexam.domain.interactor.GetBenefitsInteractor
import com.carlosgub.comunalexam.domain.repository.JsonRepository
import com.carlosgub.comunalexam.presentation.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val appModule = module {
    single<com.carlosgub.comunalexam.core.scheduler.Schedulers> {
        SchedulersImpl()
    }
}

private val mainModule = module {

    //region ViewModel
    viewModel {
        MainViewModel(get())
    }
    //endregion

    //region Interactor
    single {
        GetBenefitsInteractor(get())
    }
    //endregion

    //region Repository
    single<JsonRepository> {
        JsonRepositoryImpl(get())
    }
    //endregion

    //region Datastore
    single {
        JsonDataStore()
    }
    //endregion
}

val modules = listOf(
    appModule, mainModule
)