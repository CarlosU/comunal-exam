package com.carlosgub.comunalexam.presentation.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.carlosgub.comunalexam.R
import com.carlosgub.comunalexam.presentation.adapter.VPMainBenefitsAdapter
import com.carlosgub.comunalexam.presentation.fragments.MainBenefitsFragment
import com.carlosgub.comunalexam.presentation.viewmodel.MainViewModel
import com.carlosgub.comunalexam.presentation.viewmodel.state.MainVS
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.main_activity.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var vpMainBenefitsAdapter: VPMainBenefitsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        vpMainBenefitsAdapter = VPMainBenefitsAdapter(fragmentManager = supportFragmentManager)

        setSupportActionBar(tbMain)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        viewModelStates()
        viewModel.getBenefits()
    }

    private fun viewModelStates() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is MainVS.Benefits -> {
                    val benefitsList = it.benefitsList
                    benefitsList.forEach {
                        vpMainBenefitsAdapter.addFragment(
                            MainBenefitsFragment.newInstance(
                                ArrayList(
                                    it.benefits
                                )
                            )
                        )
                    }

                    tlMain.apply {
                        vpMain.adapter = vpMainBenefitsAdapter
                        setupWithViewPager(vpMain)
                        benefitsList.forEach {
                            val index = benefitsList.indexOf(it)
                            when (index) {
                                0 -> {
                                    tlMain.getTabAt(index)?.setIcon(R.drawable.bienestar)
                                    tlMain.getTabAt(index)?.icon?.setTint(resources.getColor(R.color.colorTabUnselected))
                                }
                                1 -> {
                                    tlMain.getTabAt(index)?.setIcon(R.drawable.diversion)
                                    tlMain.getTabAt(index)?.icon?.setTint(resources.getColor(R.color.colorTabUnselected))
                                }
                                2 -> {
                                    tlMain.getTabAt(index)?.setIcon(R.drawable.aprendizaje)
                                    tlMain.getTabAt(index)?.icon?.setTint(resources.getColor(R.color.colorTabUnselected))
                                }
                            }
                            tlMain.getTabAt(index)?.text = it.name
                        }
                        addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
                            override fun onTabReselected(tab: TabLayout.Tab?) {

                            }

                            override fun onTabSelected(tab: TabLayout.Tab?) {
                                tab?.icon?.setTint(resources.getColor(android.R.color.black))
                            }

                            override fun onTabUnselected(tab: TabLayout.Tab?) {
                                tab?.icon?.setTint(resources.getColor(R.color.colorTabUnselected))
                            }
                        })
                    }
                }
                is MainVS.Error -> {

                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                /**
                 * Do something if back toolbar button is pressed
                 */
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
