package com.carlosgub.comunalexam.presentation.viewmodel.state

import com.carlosgub.comunalexam.domain.model.BenefitsEntity

sealed class MainVS {
    class Benefits(val benefitsList: List<BenefitsEntity>) : MainVS()
    class Error(val message: String?) : MainVS()
}