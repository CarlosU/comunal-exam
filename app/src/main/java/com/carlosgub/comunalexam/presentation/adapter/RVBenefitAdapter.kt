package com.carlosgub.comunalexam.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.carlosgub.comunalexam.R
import com.carlosgub.comunalexam.domain.model.BenefitEntity
import kotlinx.android.synthetic.main.main_benefits_item.view.*


class RVBenefitAdapter :
    RecyclerView.Adapter<RVBenefitAdapter.ViewHolder>() {

    var list = mutableListOf<BenefitEntity>()

    fun addAll(benefitList:List<BenefitEntity>) {
        list.clear()
        list.addAll(benefitList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.main_benefits_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(list[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindView(benefit: BenefitEntity) {
            itemView.sdvMainBenefitsItem.setImageURI(benefit.image)
            itemView.tvMainBenefitsItemTitle.text = benefit.businessName
            itemView.tvMainBenefitsItemDescription.text = benefit.name
        }
    }
}