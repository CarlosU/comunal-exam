package com.carlosgub.comunalexam.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.carlosgub.comunalexam.core.interactor.Interactor
import com.carlosgub.comunalexam.core.platform.BaseViewModel
import com.carlosgub.comunalexam.domain.interactor.GetBenefitsInteractor
import com.carlosgub.comunalexam.presentation.viewmodel.state.MainVS

class MainViewModel(
    private val getBenefitsInteractor: GetBenefitsInteractor
) : BaseViewModel(){

    val state = MutableLiveData<MainVS>()

    fun getBenefits() {
        disposables.add(
            getBenefitsInteractor
                .execute(
                    Interactor.None
                )
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    state.value = MainVS.Benefits(it)
                }, {
                    state.value = MainVS.Error(it.message)
                })
        )
    }
}