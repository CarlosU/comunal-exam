package com.carlosgub.comunalexam.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.carlosgub.comunalexam.presentation.fragments.MainBenefitsFragment

class VPMainBenefitsAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {

    private val fragments = mutableListOf<MainBenefitsFragment>()

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    fun addFragment(fragment: MainBenefitsFragment) {
        fragments.add(fragment)
    }
}