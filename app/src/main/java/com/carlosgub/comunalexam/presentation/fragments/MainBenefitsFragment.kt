package com.carlosgub.comunalexam.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.carlosgub.comunalexam.R
import com.carlosgub.comunalexam.domain.model.BenefitEntity
import com.carlosgub.comunalexam.presentation.adapter.RVBenefitAdapter
import kotlinx.android.synthetic.main.main_benefits_fragment.*

class MainBenefitsFragment : Fragment() {

    private val rvBenefitAdapter =  RVBenefitAdapter()

    companion object {
        const val ARG_BENEFIT = "arg_benefit"
        fun newInstance(benefits: ArrayList<BenefitEntity>): MainBenefitsFragment =
            MainBenefitsFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_BENEFIT, benefits)
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.main_benefits_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            it.getParcelableArrayList<BenefitEntity>(ARG_BENEFIT)?.let{
                rvBenefitAdapter.addAll(it)
            }
        }

        rvBenefits.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = rvBenefitAdapter
        }

    }
}
